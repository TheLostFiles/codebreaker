#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cctype>

using namespace std;


int main()
{
	const int MAX_WRONG = 8;  //maximum number of incorrect guesses allowed
	int wrong = 0; //number of incorrect guesses
	int right = 0; // number of correct guesses
	int SimNum = 1;
	
	
	vector<string> words;   //collection of possible words to guess
	words.push_back("APPENDIX");
	words.push_back("TEMPTATION");
	words.push_back("FREQUENCY");
	words.push_back("INFRASTRUCTURE");// Create a collection of 10 words you had written down manually
	words.push_back("HYPOTHESIS");
	words.push_back("PARADOX");
	words.push_back("SAUSAGE");
	words.push_back("LITERATURE");
	words.push_back("SUPPLEMENTARY");
	words.push_back("GHOSTWRITER");
	


	
	cout << "Welcome to CodeBreak 2.0\n";  // Display Title of the program to the user
	

	cout << "What is your name agent?\n"; // Ask the recruit to log in using their name
	
	
	string name; // Hold the recruit's name in a var, and address them by it throughout the simulation.
	cin >> name;
	
	cout << "\nHi "<< name << "\nCodeBreaker 2.0 is Program where you are given different codes that you must break.\nBreak the code in order to move on.\n"; // Display an overview of what Keywords II is to the recruit
	system("pause");
	
	cout << "This is sim " << SimNum << "\n";
	
	do
	{

		srand(static_cast<unsigned int>(time(0))); // grabs time as the seed for random
		random_shuffle(words.begin(), words.end()); // grabs a random word

		const string THE_WORD = words[0]; //word to guess
		string soFar(THE_WORD.size(), '-'); //word guessed so far
		string used = ""; //letters already guessed
		
		while ((wrong < MAX_WRONG) && (soFar != THE_WORD)) // checks if the while should still run 
		{
			
			
			cout << "\nYou have " << (MAX_WRONG - wrong);// Tell recruit how many incorrect guesses he or she has left
			cout << " incorrect guesses left.\n";// Tell recruit how many incorrect guesses he or she has left
			cout << "\nYou've used the following letters:\n" << used << endl;// Show recruit the letters he or she has guessed
			cout << "\nSo far, the word is:\n" << soFar << endl;// Show player how much of the secret word he or she has guessed
            
			char guess;
			cout << "\n\nEnter your guess: ";// Get recruit's next guess
			cin >> guess;
			guess = toupper(guess); //make uppercase since secret word in uppercase
			while (used.find(guess) != string::npos) // checks if the letter is not in the word
			{
				cout << "\nYou've already guessed " << guess << endl;
				cout << "Enter your guess: ";// Get recruit �s guess
				cin >> guess;
				guess = toupper(guess);// Add the new guess to the group of used letters
			}
	
			used += guess;

			if (THE_WORD.find(guess) != string::npos)// checks if the letter is not in the word
			{
				cout << "That's right! " << guess << " is in the word.\n";// Tell the recruit the guess is correct
				//update soFar to include newly guessed letter
				for (int i = 0; i < THE_WORD.length(); ++i) // for loop checking if the size and number of letters is right 
				{
					if (THE_WORD[i] == guess)
					{
						soFar[i] = guess;//          Update the word guessed so far with the new letter
					}
				}
			}
			else// Otherwise
			{
				cout << "Sorry, " << guess << " isn't in the word.\n";// Tell the recruit the guess is incorrect
				++wrong;// Increment the number of incorrect guesses the recruit has made
			}
		}
			if (wrong == MAX_WRONG)
			{
				cout << "\nYou Didn't crack and you fail. GOODBYE\n";// Tell the recruit that he or she has failed the Keywords II course.
				system("pause");
				right = 3;
			}
			else
			{
				cout << "\nYou guessed it! Good Job You have done it!"; // Congratulate the recruit on guessing the secret words
			
			}
		
		right++;
		cout << "\nThe word was " << THE_WORD << endl;
		system("pause"); // pauses the simulation
		system("CLS");// clears the screen

		if(right == 3) // checks if right is 3
		{
			string again;
			cout << "Good Job " << name << "!! You did it!\n"; // Congratulate the recruit on guessing the secret words
			cout << "Would you like to do it again?\n"; // Ask the recruit if they would like to run the simulation again
			cin >> again; // takes in the cin
				if (again == "yes") 
				{
					SimNum++;// Increment the number of simulations ran counter
					right = 0;// Move program execution back up to // Display the simulation # is starting to the recruit. 
					system("CLS");
				}
				else // otherwise
				{
					
				}
			
		}
		
	}while (right < 3);// Pick new 3 random words from your collection as the secret code word the recruit has to guess. 

	
	
	return 0;
}

